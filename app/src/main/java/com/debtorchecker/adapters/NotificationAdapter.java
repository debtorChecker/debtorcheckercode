package com.debtorchecker.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.debtorchecker.R;
import com.debtorchecker.models.NotificationModel;

import java.util.ArrayList;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    /*
     * Initialize LayoutInflater
     * Initialize Context mContext
     * */
    private ArrayList<NotificationModel> mArrayList;
    private Activity mActivity;


    public NotificationAdapter(Activity mActivity, ArrayList<NotificationModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final NotificationModel mModel = mArrayList.get(position);

        holder.txtTitleTV.setText(mModel.getTitle());
        holder.txtDescriptionTV.setText(mModel.getDescription());
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTitleTV, txtDescriptionTV;
        public LinearLayout itemLL;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTitleTV = itemView.findViewById(R.id.txtTitleTV);
            txtDescriptionTV = itemView.findViewById(R.id.txtDescriptionTV);
            itemLL = itemView.findViewById(R.id.itemLL);
        }

    }
}
