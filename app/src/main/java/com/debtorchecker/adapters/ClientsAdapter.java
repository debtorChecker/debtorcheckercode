package com.debtorchecker.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.debtorchecker.R;
import com.debtorchecker.models.ClientsModel;

import java.util.ArrayList;


public class ClientsAdapter extends RecyclerView.Adapter<ClientsAdapter.ViewHolder> {
    /*
     * Initialize LayoutInflater
     * Initialize Context mContext
     * */
    private ArrayList<ClientsModel> mArrayList;
    private Activity mActivity;


    public ClientsAdapter(Activity mActivity, ArrayList<ClientsModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_clients, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ClientsModel mModel = mArrayList.get(position);

        holder.txtClientNameTV.setText(mModel.getClientName());
        holder.txtClientAddressTV.setText(mModel.getClientState());
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtClientNameTV, txtClientAddressTV;
        public LinearLayout itemLL;

        public ViewHolder(View itemView) {
            super(itemView);
            txtClientAddressTV = itemView.findViewById(R.id.txtClientAddressTV);
            txtClientNameTV = itemView.findViewById(R.id.txtClientNameTV);
            itemLL = itemView.findViewById(R.id.itemLL);
        }

    }
}
