package com.debtorchecker.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.debtorchecker.R;
import com.debtorchecker.activities.MyInvoicesDetailsActivity;
import com.debtorchecker.models.MyInvoicesModel;

import java.util.ArrayList;


public class MyInvoicesDetailsAdapter extends RecyclerView.Adapter<MyInvoicesDetailsAdapter.ViewHolder> {
    /*
     * Initialize LayoutInflater
     * Initialize Context mContext
     * */
    private ArrayList<MyInvoicesModel> mArrayList;
    private Activity mActivity;


    public MyInvoicesDetailsAdapter(Activity mActivity, ArrayList<MyInvoicesModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_invoices_details, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final MyInvoicesModel mModel = mArrayList.get(position);
        holder.itemLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtInvoiceTitleTV, txtLocationTV,txtActiveInvoiceTV,txtTotalOutStandingTV;
        public CardView itemLL;

        public ViewHolder(View itemView) {
            super(itemView);
            txtInvoiceTitleTV = itemView.findViewById(R.id.txtClientAddressTV);
            txtLocationTV = itemView.findViewById(R.id.txtLocationTV);
            txtActiveInvoiceTV = itemView.findViewById(R.id.txtActiveInvoiceTV);
            txtTotalOutStandingTV = itemView.findViewById(R.id.txtTotalOutStandingTV);
            itemLL = itemView.findViewById(R.id.itemLL);
        }

    }
}
