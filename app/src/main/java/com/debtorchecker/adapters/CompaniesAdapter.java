package com.debtorchecker.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.debtorchecker.R;
import com.debtorchecker.models.CompaniesModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class CompaniesAdapter extends RecyclerView.Adapter<CompaniesAdapter.ViewHolder> {
    /*
     * Initialize LayoutInflater
     * Initialize Context mContext
     * */
    private ArrayList<CompaniesModel> mArrayList;
    private Activity mActivity;


    public CompaniesAdapter(Activity mActivity, ArrayList<CompaniesModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_companies, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final CompaniesModel mModel = mArrayList.get(position);

        holder.txtCompanyNameTV.setText(mModel.getCompanyName());
        holder.txtCompanyAddressTV.setText(mModel.getCompanyState());
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtCompanyNameTV, txtCompanyAddressTV;
        public LinearLayout itemLL;

        public ViewHolder(View itemView) {
            super(itemView);
            txtCompanyAddressTV = itemView.findViewById(R.id.txtCompanyAddressTV);
            txtCompanyNameTV = itemView.findViewById(R.id.txtCompanyNameTV);
            itemLL = itemView.findViewById(R.id.itemLL);
        }

    }
}
