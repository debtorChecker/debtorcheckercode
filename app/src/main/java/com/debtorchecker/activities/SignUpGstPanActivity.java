package com.debtorchecker.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.debtorchecker.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpGstPanActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpGstPanActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SignUpGstPanActivity.this;


    /*
     * Widgets
     * */

    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.imgNextIV)
    ImageView imgNextIV;
    @BindView(R.id.txtComapanyNameTV)
    TextView txtComapanyNameTV;
    @BindView(R.id.txtAddressTV)
    TextView txtAddressTV;
    @BindView(R.id.editGstNumET)
    EditText editGstNumET;
    @BindView(R.id.editPanNumET)
    EditText editPanNumET;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_gst_pan);
        ButterKnife.bind(this);
    }


    /*
     * Widget Click listner
     * */
    @OnClick({R.id.imgBackIV, R.id.btnNextB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.btnNextB:
                performNextClick();
                break;
        }
    }

    private void performNextClick() {
        if (isValidate())
            startActivity(new Intent(mActivity, SignUpCompleteActivity.class));
    }


    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editGstNumET.getText().toString().trim().equals("") && editPanNumET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_at_least));
            flag = false;
        }
        return flag;
    }
}
