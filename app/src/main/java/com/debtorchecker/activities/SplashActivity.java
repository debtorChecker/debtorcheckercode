package com.debtorchecker.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.debtorchecker.R;

public class SplashActivity extends BaseActivity {

    /**
     * Splash Close Time
     */
    public final int SPLASH_TIME_OUT = 2000;

    /**
     * Getting the Current Class Name
     */
    String TAG = SplashActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = SplashActivity.this;


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setUpSplash();
    }

    private void setUpSplash() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(mActivity, SignInActivity.class));
                finish();

            }
        }, SPLASH_TIME_OUT);
    }
}
