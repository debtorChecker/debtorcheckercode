package com.debtorchecker.activities;

import androidx.annotation.NonNull;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.debtorchecker.R;
import com.tooltip.OnClickListener;
import com.tooltip.Tooltip;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddInvoiceClientActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = AddInvoiceClientActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = AddInvoiceClientActivity.this;


    /*
     * Widgets
     * */

    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.btnNextB)
    Button btnNextB;
    @BindView(R.id.imgAddClientIV)
    ImageView imgAddClientIV;
    @BindView(R.id.imgQuestionMarkIV)
    ImageView imgQuestionMarkIV;
    @BindView(R.id.txtSearchClientTV)
    TextView txtSearchClientTV;

    /*
     * Initialize
     * */


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_invoice);
        //Butterknife
        ButterKnife.bind(this);
    }

    /*
     * Widget Click listner
     * */
    @OnClick({R.id.imgBackIV, R.id.txtSearchClientTV, R.id.btnNextB, R.id.imgQuestionMarkIV, R.id.imgAddClientIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.txtSearchClientTV:
                performSearchClick();
                break;
            case R.id.btnNextB:
                performNextClick();
                break;
            case R.id.imgQuestionMarkIV:
                performQuestionMarkClick();
                break;
            case R.id.imgAddClientIV:
                showAddClientDialog();
                break;

        }
    }



    private void performSearchClick() {
        startActivity(new Intent(mActivity, SearchInvoiceClientActivity.class));
    }
    private void performNextClick() {
        startActivity(new Intent(mActivity, AddUserInvoiceActivity.class));
    }


    private void performQuestionMarkClick() {
        Tooltip tooltip = new Tooltip.Builder(imgQuestionMarkIV)
                .setText(getString(R.string.can_not_find_client))
                .setGravity(Gravity.LEFT)
                .setCornerRadius(R.dimen._5sdp)
                .setTextColor(getResources().getColor(R.color.colorWhite))
                .setBackgroundColor(getResources().getColor(R.color.colorPrimary))
                .show();

        tooltip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(@NonNull Tooltip tooltip) {
                tooltip.dismiss();
            }
        });
    }


    public void showAddClientDialog() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_add_client);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        EditText editClientNameET = alertDialog.findViewById(R.id.editClientNameET);
        EditText editGstNumET = alertDialog.findViewById(R.id.editGstNumET);
        EditText editPanNumET = alertDialog.findViewById(R.id.editPanNumET);

        Button btnAddClientB = alertDialog.findViewById(R.id.btnAddClientB);

        btnAddClientB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editClientNameET.getText().toString().trim().equals("")) {
                    editClientNameET.setError(getString(R.string.please_enter_client_name));
                }else if (editGstNumET.getText().toString().trim().equals("") && editPanNumET.getText().toString().trim().equals("")) {
                    editGstNumET.setError(getString(R.string.please_enter_at_least));
                    editPanNumET.setError(getString(R.string.please_enter_at_least));
                } else {
                    alertDialog.dismiss();
                    showToast(mActivity, "Client Added Successfully.");
                }
            }
        });
        alertDialog.show();
    }


}
