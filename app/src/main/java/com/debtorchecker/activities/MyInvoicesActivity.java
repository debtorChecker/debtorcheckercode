package com.debtorchecker.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.debtorchecker.R;
import com.debtorchecker.adapters.CompaniesAdapter;
import com.debtorchecker.adapters.MyInvoicesAdapter;
import com.debtorchecker.models.CompaniesModel;
import com.debtorchecker.models.MyInvoicesModel;
import com.debtorchecker.utils.Constants;
import com.tooltip.OnClickListener;
import com.tooltip.Tooltip;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyInvoicesActivity extends AppCompatActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = MyInvoicesActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = MyInvoicesActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.imgHomeIV)
    ImageView imgHomeIV;
    @BindView(R.id.imgAddInvoiceIV)
    ImageView imgAddInvoiceIV;
    @BindView(R.id.imgQuestionMarkIV)
    ImageView imgQuestionMarkIV;
    @BindView(R.id.myInvoiceRecyclerViewRV)
    RecyclerView myInvoiceRecyclerViewRV;

    /*
    * Initialize...
    * */
    MyInvoicesAdapter mMyInvoicesAdapter;
    ArrayList<MyInvoicesModel> mMyInvoicesArrayList = new ArrayList<>();

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_invoices);
        //Butter Knife
        ButterKnife.bind(this);
        setAdapter();
    }



    /*
     * Widget Click listner
     * */
    @OnClick({R.id.imgHomeIV,R.id.imgAddInvoiceIV,R.id.imgQuestionMarkIV,R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgHomeIV:
                performHomeClick();
                break;
            case R.id.imgAddInvoiceIV:
                performAddInvoiceClick();
                break;
            case R.id.imgQuestionMarkIV:
                performQuestionMarkClick();
                break;
            case R.id.imgBackIV:
                onBackPressed();
                break;

        }
    }

    private void performHomeClick() {
        Intent mIntent = new Intent(mActivity, DashboardActivity.class);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mIntent);
        finish();
    }

    private void performAddInvoiceClick() {
        startActivity(new Intent(mActivity,AddUserInvoiceActivity.class));
    }


    private void setAdapter() {

        for (int i = 0; i < 20; i++){
            MyInvoicesModel mModel = new MyInvoicesModel();

            mMyInvoicesArrayList.add(mModel);
        }
        //Comments Adapter
        myInvoiceRecyclerViewRV.setNestedScrollingEnabled(false);
        mMyInvoicesAdapter = new MyInvoicesAdapter(mActivity, mMyInvoicesArrayList);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(mActivity);
        myInvoiceRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        myInvoiceRecyclerViewRV.setAdapter(mMyInvoicesAdapter);
    }


        private void performQuestionMarkClick() {
        Tooltip tooltip = new Tooltip.Builder(imgQuestionMarkIV)
                .setText(getString(R.string.can_not_find_your_invoices))
                .setGravity(Gravity.LEFT)
                .setCornerRadius(R.dimen._5sdp)
                .setTextColor(getResources().getColor(R.color.colorWhite))
                .setBackgroundColor(getResources().getColor(R.color.colorPrimary))
                .show();

        tooltip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(@NonNull Tooltip tooltip) {
                tooltip.dismiss();
            }
        });
    }


}
