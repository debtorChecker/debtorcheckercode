package com.debtorchecker.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.debtorchecker.R;
import com.debtorchecker.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TermConditionsActivity extends AppCompatActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = TermConditionsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = TermConditionsActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.imgHomeIV)
    ImageView imgHomeIV;



    /*
     * Initialize...Objects
     * */
    String strActivityType = "";

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_conditions);
        //Butter Knife
        ButterKnife.bind(this);
        //Get Intent Data
        getIntentData();
    }

    private void getIntentData() {
        if (getIntent() != null){
            strActivityType = getIntent().getStringExtra(Constants.TYPE_TERM_CONTIONS);
            if (strActivityType.equals(Constants.TYPE_NAV_MENU)){
                imgHomeIV.setVisibility(View.VISIBLE);
            }else if (strActivityType.equals(Constants.TYPE_SIGN_UP)){
                imgHomeIV.setVisibility(View.GONE);
            }
        }
    }

    /*
     * Widgets Click Listner
     * */
    @OnClick({R.id.imgBackIV, R.id.imgHomeIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.imgHomeIV:
                performHomeClick();
                break;

        }
    }

    private void performHomeClick() {
        Intent mIntent = new Intent(mActivity, DashboardActivity.class);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mIntent);
        finish();
    }

}
