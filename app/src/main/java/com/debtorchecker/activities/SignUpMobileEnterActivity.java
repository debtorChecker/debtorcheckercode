package com.debtorchecker.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.debtorchecker.R;
import com.debtorchecker.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpMobileEnterActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpMobileEnterActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = SignUpMobileEnterActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.btnNextB)
    Button btnNextB;
    @BindView(R.id.editMobileNoET)
    EditText editMobileNoET;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_mobile_enter);
        //ButterKnife
        ButterKnife.bind(this);
        //Set Up Mobile Number
        setUpMobileNumber();
    }

    private void setUpMobileNumber() {
        editMobileNoET.setText("+91");
        editMobileNoET.setSelection(editMobileNoET.getText().toString().trim().length());
    }


    /*
     * Widget Click listnerbtnNextB
     * */
    @OnClick({R.id.imgBackIV, R.id.btnNextB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
               onBackPressed();
                break;
            case R.id.btnNextB:
                performNextClick();
                break;
        }
    }

    private void performNextClick() {
        if (isValidate()){
            Intent mIntent = new Intent(mActivity, SignUpOtpVerifyActivity.class);
            mIntent.putExtra(Constants.PHONE_NUMBER,editMobileNoET.getText().toString().trim());
            startActivity(mIntent);
            finish();
        }
    }



    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editMobileNoET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_mobile_number));
            flag = false;
        } else if (editMobileNoET.getText().toString().trim().length() != 13) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_mobile_number));
            flag = false;
        }
        return flag;
    }
}
