package com.debtorchecker.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.debtorchecker.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MarketResearchAddressActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = MarketResearchAddressActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = MarketResearchAddressActivity.this;


    /*
     * Widgets
     * */

    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.editAddressLine1ET)
    EditText editAddressLine1ET;
    @BindView(R.id.editAddressLine2ET)
    EditText editAddressLine2ET;
    @BindView(R.id.editDistrictET)
    EditText editDistrictET;
    @BindView(R.id.editStateET)
    EditText editStateET;
    @BindView(R.id.btnAddCompanyB)
    Button btnAddCompanyB;
    @BindView(R.id.txtSkipB)
    TextView txtSkipB;


    /*
     * Initialize
     * */
    String strState = "";


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_research_address);
        //Butter knife
        ButterKnife.bind(this);
    }


    /*
     * Widget Click listner
     * */
    @OnClick({R.id.imgBackIV, R.id.btnAddCompanyB,R.id.txtSkipB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.btnAddCompanyB:
                performCompanyClick();
                break;

            case R.id.txtSkipB:
                performSkipClick();
                break;

        }
    }

    private void performSkipClick() {
        startActivity(new Intent(mActivity,MarketResearchCompleteActivity.class));
    }

    private void performCompanyClick() {
        if (isValidate()){
            startActivity(new Intent(mActivity,MarketResearchCompleteActivity.class));
        }
    }

    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editAddressLine1ET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_market_research_address));
            flag = false;
        } else if (editDistrictET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_district));
            flag = false;
        } else if (editStateET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_state));
            flag = false;
        }
        return flag;
    }


}
