package com.debtorchecker.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.debtorchecker.R;
import com.tooltip.OnClickListener;
import com.tooltip.Tooltip;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddUserInvoiceActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = AddUserInvoiceActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = AddUserInvoiceActivity.this;


    /*
     * Widgets
     * */

    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.imgNotification)
    ImageView imgNotification;
    @BindView(R.id.imgHomeIV)
    ImageView imgHomeIV;
    @BindView(R.id.editInvoiceNumberET)
    EditText editInvoiceNumberET;
    @BindView(R.id.txtInvoiceDateTV)
    TextView txtInvoiceDateTV;
    @BindView(R.id.editInvoiceAmountET)
    EditText editInvoiceAmountET;
    @BindView(R.id.btnAddClientB)
    Button btnAddClientB;
    @BindView(R.id.imgQuestionMarkIV)
    ImageView imgQuestionMarkIV;


    /*
     * Initialize
     * */
    String strState = "";


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user_invoice);
        //Butter knife
        ButterKnife.bind(this);
    }

    /*
     * Widget Click listner
     * */
    @OnClick({R.id.imgBackIV, R.id.imgNotification, R.id.imgHomeIV, R.id.txtInvoiceDateTV, R.id.btnAddClientB, R.id.imgQuestionMarkIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.imgNotification:
                performNotificationClick();
                break;
            case R.id.imgHomeIV:
                performHomeClick();
                break;
            case R.id.txtInvoiceDateTV:
                performDateClick();
                break;
            case R.id.btnAddClientB:
                performAddInvoiceClick();
                break;
            case R.id.imgQuestionMarkIV:
                performQuestionMarkClick();
                break;

        }
    }

    private void performDateClick() {
        showDatePickerDialog(mActivity,txtInvoiceDateTV);
    }

    private void performNotificationClick() {
        showToast(mActivity,"Coming Soon...");
    }

    private void performHomeClick() {
        Intent mIntent = new Intent(mActivity, DashboardActivity.class);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mIntent);
        finish();
    }

    private void performAddInvoiceClick() {
        if (isValidate()){
            startActivity(new Intent(mActivity,InvoiceCompletedAcitivity.class));
        }
    }

    private void performQuestionMarkClick() {
        Tooltip tooltip = new Tooltip.Builder(imgQuestionMarkIV)
                .setText(getString(R.string.can_not_find_invoice))
                .setGravity(Gravity.LEFT)
                .setCornerRadius(R.dimen._5sdp)
                .setTextColor(getResources().getColor(R.color.colorWhite))
                .setBackgroundColor(getResources().getColor(R.color.colorPrimary))
                .show();

        tooltip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(@NonNull Tooltip tooltip) {
                tooltip.dismiss();
            }
        });
    }


    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editInvoiceNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_invoice_number));
            flag = false;
        } else if (txtInvoiceDateTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_invoice_date));
            flag = false;
        } else if (editInvoiceAmountET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_invoice_amount));
            flag = false;
        }
        return flag;
    }
}
