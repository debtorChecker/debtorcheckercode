package com.debtorchecker.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.debtorchecker.R;
import com.debtorchecker.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpPasswordActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpPasswordActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = SignUpPasswordActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.editReEnterPasswordET)
    EditText editReEnterPasswordET;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.btnNextB)
    Button btnNextB;
    @BindView(R.id.checkTermConditionCB)
    CheckBox checkTermConditionCB;
    @BindView(R.id.txtTermConditionTV)
    TextView txtTermConditionTV;
    @BindView(R.id.checkConfirmPasswordHideShowCB)
    CheckBox checkConfirmPasswordHideShowCB;
    @BindView(R.id.checkPasswordHideShowCB)
    CheckBox checkPasswordHideShowCB;

    /*
    * Initialize Objects...
    * */
    boolean isTermConditions = false;


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_password);
        //ButterKnife
        ButterKnife.bind(this);
        //Set Up Checkbox Selector
        setUpTermConditionCheckbox();
        //Set up Password Hide/Show
        performPasswordHideShowClick();
        performConfirmPasswordHideShowClick();
    }

    private void setUpTermConditionCheckbox() {
        checkTermConditionCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isTermConditions = b;
            }
        });
    }

    /*
     * Widget Click listner
     * */
    @OnClick({R.id.imgBackIV, R.id.btnNextB, R.id.txtTermConditionTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.btnNextB:
                performNextClick();
                break;
            case R.id.txtTermConditionTV:
                performTermConditionsClick();
                break;
        }
    }

    private void performTermConditionsClick() {
        Intent mIntent = new Intent(mActivity, TermConditionsActivity.class);
        mIntent.putExtra(Constants.TYPE_TERM_CONTIONS, Constants.TYPE_SIGN_UP);
        startActivity(mIntent);
    }

    private void performNextClick() {
        if (isValidate()) {
            startActivity(new Intent(mActivity, SignUpCompanyActivity.class));
        }
    }


    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_the_password));
            flag = false;
        } else if (editReEnterPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_re_enter_the_password));
            flag = false;
        } else if (!editPasswordET.getText().toString().trim().equals(editReEnterPasswordET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_match_the_re_enter));
            flag = false;
        }else if (isTermConditions == false) {
            showAlertDialog(mActivity, getString(R.string.please_read_term_condition));
            flag = false;
        }
        return flag;
    }

    /*
     *
     * Password Hide & Show
     * */
    private void performPasswordHideShowClick() {
        checkPasswordHideShowCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    // show password
                    editPasswordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    // hide password
                    editPasswordET.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });
    }
    /*
     *
     * Confirm Password Hide & Show
     * */
    private void performConfirmPasswordHideShowClick() {
        checkConfirmPasswordHideShowCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    // show password
                    editReEnterPasswordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    // hide password
                    editReEnterPasswordET.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });
    }


}
