package com.debtorchecker.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.debtorchecker.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InvoiceCompletedAcitivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = InvoiceCompletedAcitivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = InvoiceCompletedAcitivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.homeLL)
    LinearLayout homeLL;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.btnAddAnotherB)
    Button btnAddAnotherB;


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_completed_acitivity);
        ButterKnife.bind(this);
    }

    /*
     * Widget Click listner
     * */
    @OnClick({R.id.imgBackIV, R.id.homeLL, R.id.btnAddAnotherB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.homeLL:
                performHomeClick();
                break;
            case R.id.btnAddAnotherB:
                performAddAnotherClick();
                break;
        }
    }

    private void performHomeClick() {
        Intent mIntent = new Intent(mActivity, DashboardActivity.class);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mIntent);
        finish();
    }

    private void performAddAnotherClick() {
        showAntoherInvoiceAlertDialog();
    }


    /*
     *
     * Add Another Invoice Alert Dialog
     * */
    public void showAntoherInvoiceAlertDialog() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_add_another_invoice);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView btnSameClientB = alertDialog.findViewById(R.id.btnSameClientB);
        TextView btnDiffrentClientB = alertDialog.findViewById(R.id.btnDiffrentClientB);

        btnSameClientB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent mIntent = new Intent(mActivity, AddUserInvoiceActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(mIntent);
                finish();
            }
        });
        btnDiffrentClientB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent mIntent = new Intent(mActivity, AddInvoiceClientActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(mIntent);
                finish();
            }
        });
        alertDialog.show();
    }


}
