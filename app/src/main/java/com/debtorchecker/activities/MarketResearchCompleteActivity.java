package com.debtorchecker.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.debtorchecker.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MarketResearchCompleteActivity extends AppCompatActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = MarketResearchCompleteActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = MarketResearchCompleteActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.btnNextB)
    Button btnNextB;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;



    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_research_complete);
        ButterKnife.bind(this);
    }


    /*
     * Widget Click listner
     * */
    @OnClick({R.id.imgBackIV, R.id.btnNextB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.btnNextB:
                performNextClick();
                break;
        }
    }

    private void performNextClick() {
        Intent mIntent = new Intent(mActivity, DashboardActivity.class);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mIntent);
        finish();
    }

}
