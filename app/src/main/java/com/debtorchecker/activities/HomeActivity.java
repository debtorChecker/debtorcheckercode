package com.debtorchecker.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.debtorchecker.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = HomeActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = HomeActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.imgMenuIV)
    ImageView imgMenuIV;
    @BindView(R.id.addInvoiceLL)
    LinearLayout addInvoiceLL;
    @BindView(R.id.myInvoicesLL)
    LinearLayout myInvoicesLL;
    @BindView(R.id.addReminderLL)
    LinearLayout addReminderLL;
    @BindView(R.id.myRemindersLL)
    LinearLayout myRemindersLL;
    @BindView(R.id.marketResearchLL)
    LinearLayout marketResearchLL;
    @BindView(R.id.searchCompanyLL)
    LinearLayout searchCompanyLL;
    @BindView(R.id.feedbackLL)
    LinearLayout feedbackLL;
    @BindView(R.id.contactUsLL)
    LinearLayout contactUsLL;


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //ButterKnife
        ButterKnife.bind(this);
    }


    /*
     * Widget Click listner
     * */
    @OnClick({R.id.imgMenuIV, R.id.addInvoiceLL, R.id.myInvoicesLL, R.id.addReminderLL, R.id.myRemindersLL, R.id.marketResearchLL, R.id.searchCompanyLL, R.id.feedbackLL, R.id.contactUsLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgMenuIV:
                break;
            case R.id.addInvoiceLL:
                startActivity(new Intent(mActivity, AddInvoiceClientActivity.class));
                break;
            case R.id.myInvoicesLL:
                startActivity(new Intent(mActivity,MyInvoicesActivity.class));
                break;
            case R.id.addReminderLL:
                startActivity(new Intent(mActivity,AddReminderActivity.class));
                break;
            case R.id.myRemindersLL:
                startActivity(new Intent(mActivity,MyRemindersActivity.class));
                break;
            case R.id.marketResearchLL:
                startActivity(new Intent(mActivity, MarketResearchClientActivity.class));
                break;
            case R.id.searchCompanyLL:
                startActivity(new Intent(mActivity,SearchCompanyActivity.class));
                break;
            case R.id.feedbackLL:
                startActivity(new Intent(mActivity,FeedbackActivity.class));
                break;
            case R.id.contactUsLL:
                startActivity(new Intent(mActivity,ContactUsActivity.class));
                break;
        }
    }

}
