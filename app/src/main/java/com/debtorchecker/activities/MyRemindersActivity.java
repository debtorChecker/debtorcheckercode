package com.debtorchecker.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.debtorchecker.R;

public class MyRemindersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_reminders);
    }
}
