package com.debtorchecker.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.debtorchecker.R;
import com.debtorchecker.utils.Constants;
import com.google.android.material.navigation.NavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DashboardActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = DashboardActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = DashboardActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.imgNotification)
    ImageView imgNotification;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.imgHomeIV)
    ImageView imgHomeIV;

    @BindView(R.id.imgAddInvoiceIV)
    ImageView imgAddInvoiceIV;

    @BindView(R.id.imgMarketResearchIV)
    ImageView imgMarketResearchIV;

    @BindView(R.id.imgAddReminderIV)
    ImageView imgAddReminderIV;

    @BindView(R.id.imgSearchComapnayIV)
    ImageView imgSearchComapnayIV;

    @BindView(R.id.imgLedgerAccountIV)
    ImageView imgLedgerAccountIV;

    @BindView(R.id.imgContactUsIV)
    ImageView imgContactUsIV;

    @BindView(R.id.myMarketResearchLL)
    LinearLayout myMarketResearchLL;
    @BindView(R.id.myInvoiceLL)
    LinearLayout myInvoiceLL;
    @BindView(R.id.myReminderLL)
    LinearLayout myReminderLL;
    @BindView(R.id.myLedgerLL)
    LinearLayout myLedgerLL;

    /*
     * Drawer Widgets
     * */
    @BindView(R.id.aboutUsLL)
    LinearLayout aboutUsLL;
    @BindView(R.id.updateProfileLL)
    LinearLayout updateProfileLL;
    @BindView(R.id.updateClientProfileLL)
    LinearLayout updateClientProfileLL;
    @BindView(R.id.contactUsLL)
    LinearLayout contactUsLL;
    @BindView(R.id.feedbackUsLL)
    LinearLayout feedbackUsLL;
    @BindView(R.id.privacyPolicyLL)
    LinearLayout privacyPolicyLL;
    @BindView(R.id.termConditionLL)
    LinearLayout termConditionLL;
    @BindView(R.id.logoutLL)
    LinearLayout logoutLL;
    @BindView(R.id.txtVersionTV)
    TextView txtVersionTV;




    /*
     * Activity Override method
     * #onActivityCreated
     * */


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        //ButterKnife
        ButterKnife.bind(this);
        //Set Navigation Drawer
        setNavigationDrawer();
        //Set Navigation Version
        setNavigationVersion();
    }

    /*
    * Set Up App Version
    * */
    private void setNavigationVersion() {
        try {
            String currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            txtVersionTV.setText("Version(" + currentVersion + ")");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    /*
     * Set Up Navigation Drawer
     * */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setNavigationDrawer() {
        ImageView imgNavMenuIV = findViewById(R.id.imgNavMenuIV);

        imgNavMenuIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    /*
     * Override()
     * onBackPress
     * */
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    /*
     * Set Widgets Click lister
     * */
    @OnClick({R.id.imgNavMenuIV, R.id.imgNotification, R.id.imgAddInvoiceIV, R.id.imgMarketResearchIV,
            R.id.imgAddReminderIV, R.id.imgSearchComapnayIV, R.id.imgLedgerAccountIV, R.id.imgContactUsIV,
            R.id.myMarketResearchLL, R.id.myInvoiceLL, R.id.myReminderLL, R.id.myLedgerLL,
            R.id.aboutUsLL,R.id.updateProfileLL,R.id.updateClientProfileLL,R.id.contactUsLL,
            R.id.feedbackUsLL, R.id.privacyPolicyLL,R.id.termConditionLL,R.id.logoutLL
    })

    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgMenuIV:
                performNavigationMenuClick();
                break;
            case R.id.imgNotification:
                performNotificationClick();
                break;
            case R.id.imgHomeIV:
                performHomeClick();
                break;
            case R.id.imgAddInvoiceIV:
                performAddInvoiceClick();
                startActivity(new Intent(mActivity, AddInvoiceClientActivity.class));
                break;
            case R.id.imgMarketResearchIV:
                performMarketRearchClick();
                startActivity(new Intent(mActivity, MarketResearchClientActivity.class));
                break;
            case R.id.imgAddReminderIV:
                performAddReminderClick();
                startActivity(new Intent(mActivity, AddReminderActivity.class));
                break;
            case R.id.imgSearchComapnayIV:
                performSearchCompanyClick();
                startActivity(new Intent(mActivity, SearchCompanyActivity.class));
                break;
            case R.id.imgLedgerAccountIV:
                performLederAccountClick();
                startActivity(new Intent(mActivity, LedgerAccountActivity.class));
                break;
            case R.id.imgContactUsIV:
                performContactUsClick();
                startActivity(new Intent(mActivity, ContactUsActivity.class));
                break;
            case R.id.myMarketResearchLL:
                startActivity(new Intent(mActivity, MyMarketResearchActivity.class));
                break;
            case R.id.myInvoiceLL:
                startActivity(new Intent(mActivity, MyInvoicesActivity.class));
                break;
            case R.id.myReminderLL:
                startActivity(new Intent(mActivity, MyRemindersActivity.class));
                break;
            case R.id.myLedgerLL:
                startActivity(new Intent(mActivity, MyLedgersActivity.class));
                break;
                /*Drawer Click Listner*/
            case R.id.aboutUsLL:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(mActivity, AboutUsActivity.class));
                break;
            case R.id.updateProfileLL:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(mActivity, UpdateProfileActivity.class));
                break;
            case R.id.updateClientProfileLL:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(mActivity, UpdateClientProfileActivity.class));
                break;
            case R.id.contactUsLL:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(mActivity, ContactUsActivity.class));
                break;
            case R.id.feedbackUsLL:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(mActivity, FeedbackActivity.class));
                break;
            case R.id.privacyPolicyLL:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(mActivity, PrivacyPolicyActivity.class));
                break;
            case R.id.termConditionLL:
                drawer.closeDrawer(GravityCompat.START);
                Intent mIntent = new Intent(mActivity, TermConditionsActivity.class);
                mIntent.putExtra(Constants.TYPE_TERM_CONTIONS, Constants.TYPE_NAV_MENU);
                startActivity(mIntent);
                break;
            case R.id.logoutLL:
                drawer.closeDrawer(GravityCompat.START);
                performLogoutCLick();
                break;
        }
    }


    private void performNotificationClick() {
        startActivity(new Intent(mActivity, NotificationActivity.class));
    }

    private void performHomeClick() {

    }

    private void performNavigationMenuClick() {

    }


    private void performAddInvoiceClick() {
        imgAddInvoiceIV.setImageResource(R.drawable.ic_add_invoice_s);
        imgMarketResearchIV.setImageResource(R.drawable.ic_mr_us);
        imgAddReminderIV.setImageResource(R.drawable.ic_add_re_us);
        imgSearchComapnayIV.setImageResource(R.drawable.ic_sc_us);
        imgLedgerAccountIV.setImageResource(R.drawable.ic_le_us);
        imgContactUsIV.setImageResource(R.drawable.ic_cu_us);
    }

    private void performMarketRearchClick() {
        imgAddInvoiceIV.setImageResource(R.drawable.ic_add_invoice_us);
        imgMarketResearchIV.setImageResource(R.drawable.ic_mr_s);
        imgAddReminderIV.setImageResource(R.drawable.ic_add_re_us);
        imgSearchComapnayIV.setImageResource(R.drawable.ic_sc_us);
        imgLedgerAccountIV.setImageResource(R.drawable.ic_le_us);
        imgContactUsIV.setImageResource(R.drawable.ic_cu_us);
    }

    private void performAddReminderClick() {
        imgAddInvoiceIV.setImageResource(R.drawable.ic_add_invoice_us);
        imgMarketResearchIV.setImageResource(R.drawable.ic_mr_us);
        imgAddReminderIV.setImageResource(R.drawable.ic_add_re_s);
        imgSearchComapnayIV.setImageResource(R.drawable.ic_sc_us);
        imgLedgerAccountIV.setImageResource(R.drawable.ic_le_us);
        imgContactUsIV.setImageResource(R.drawable.ic_cu_us);
    }

    private void performSearchCompanyClick() {
        imgAddInvoiceIV.setImageResource(R.drawable.ic_add_invoice_us);
        imgMarketResearchIV.setImageResource(R.drawable.ic_mr_us);
        imgAddReminderIV.setImageResource(R.drawable.ic_add_re_us);
        imgSearchComapnayIV.setImageResource(R.drawable.ic_sc_s);
        imgLedgerAccountIV.setImageResource(R.drawable.ic_le_us);
        imgContactUsIV.setImageResource(R.drawable.ic_cu_us);
    }

    private void performLederAccountClick() {
        imgAddInvoiceIV.setImageResource(R.drawable.ic_add_invoice_us);
        imgMarketResearchIV.setImageResource(R.drawable.ic_mr_us);
        imgAddReminderIV.setImageResource(R.drawable.ic_add_re_us);
        imgSearchComapnayIV.setImageResource(R.drawable.ic_sc_us);
        imgLedgerAccountIV.setImageResource(R.drawable.ic_le_s);
        imgContactUsIV.setImageResource(R.drawable.ic_cu_us);
    }

    private void performContactUsClick() {
        imgAddInvoiceIV.setImageResource(R.drawable.ic_add_invoice_us);
        imgMarketResearchIV.setImageResource(R.drawable.ic_mr_us);
        imgAddReminderIV.setImageResource(R.drawable.ic_add_re_us);
        imgSearchComapnayIV.setImageResource(R.drawable.ic_sc_us);
        imgLedgerAccountIV.setImageResource(R.drawable.ic_le_us);
        imgContactUsIV.setImageResource(R.drawable.ic_cu_s);
    }


    /*
     *
     * Error Alert Dialog
     * */
    public void showLogoutDialog() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_logout);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView btnCancelB = alertDialog.findViewById(R.id.btnCancelB);
        TextView btnOkB = alertDialog.findViewById(R.id.btnOkB);

        btnCancelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        btnOkB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent mIntent = new Intent(mActivity, SignInActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(mIntent);
                finish();

            }
        });
        alertDialog.show();
    }


    private void performLogoutCLick() {
        showLogoutDialog();
    }


}
