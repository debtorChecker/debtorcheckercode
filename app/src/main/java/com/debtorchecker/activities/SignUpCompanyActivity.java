package com.debtorchecker.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.debtorchecker.R;
import com.tooltip.OnClickListener;
import com.tooltip.Tooltip;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpCompanyActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpCompanyActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = SignUpCompanyActivity.this;


    /*
     * Widgets
     * */

    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.imgNextIV)
    ImageView imgNextIV;
    @BindView(R.id.imgAddCompanyIV)
    ImageView imgAddCompanyIV;
    @BindView(R.id.imgQuestionMarkIV)
    ImageView imgQuestionMarkIV;
    @BindView(R.id.txtSearchCompanyTV)
    TextView txtSearchCompanyTV;
    @BindView(R.id.statesSpinner)
    Spinner statesSpinner;

    /*
     * Initialize
     * */
    String strState = "";
    /*
     * Initialize Typeface
     * */
    Typeface mTypeface;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_company);
        //Butterknife
        ButterKnife.bind(this);
        //Set State Spinner
        setStatesSpinner();
    }


    /*
     * Widget Click listner
     * */
    @OnClick({R.id.imgAddCompanyIV, R.id.imgBackIV, R.id.txtSearchCompanyTV, R.id.imgNextIV, R.id.imgQuestionMarkIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgAddCompanyIV:
                performAddClick();
                break;
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.txtSearchCompanyTV:
                performSearchClick();
                break;
            case R.id.imgNextIV:
                performNextClick();
                break;
            case R.id.imgQuestionMarkIV:
                performQuestionMarkClick();
                break;

        }
    }

    private void performQuestionMarkClick() {
        Tooltip tooltip = new Tooltip.Builder(imgQuestionMarkIV)
                .setText(getString(R.string.can_not_find_your_company))
                .setGravity(Gravity.LEFT)
                .setCornerRadius(R.dimen._5sdp)
                .setTextColor(getResources().getColor(R.color.colorWhite))
                .setBackgroundColor(getResources().getColor(R.color.colorPrimary))
                .show();

        tooltip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(@NonNull Tooltip tooltip) {
                tooltip.dismiss();
            }
        });
    }

    private void performNextClick() {
        startActivity(new Intent(mActivity, AddCompanyAddressActivity.class));
    }

    private void performSearchClick() {
        startActivity(new Intent(mActivity, SearchCompanyActivity.class));
    }

    private void performAddClick() {
        showAddComapanyDialog();
    }


    /*
     * Setup States Spinner
     * */
    private void setStatesSpinner() {

        ArrayList<String> mStatesArrayList = new ArrayList<>();
        for (int i = 0; i < getAllStatesOfIndia().length; i++) {
            mStatesArrayList.add(getAllStatesOfIndia()[i]);
        }

        Collections.sort(mStatesArrayList);
        mStatesArrayList.add(0, "Select State");
        // Spinner click listener
        statesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                strState = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mStatesArrayList) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                mTypeface = Typeface.createFromAsset(getAssets(), "ArialRegular.ttf");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(mTypeface);
                v.setTextColor(getResources().getColor(R.color.colorText));
                v.setTextSize(18);
                return v;
            }

            public View getDropDownView(final int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(mTypeface);
                v.setTextColor(getResources().getColor(R.color.colorText));
                v.setTextSize(18);


                return v;
            }
        };

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statesSpinner.setAdapter(adapter1);
    }


    public void showAddComapanyDialog() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_add_company);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        EditText editComapanyNameET = alertDialog.findViewById(R.id.editComapanyNameET);
        EditText editGstNumET = alertDialog.findViewById(R.id.editGstNumET);
        EditText editPanNumET = alertDialog.findViewById(R.id.editPanNumET);

        Button btnAddCompanyB = alertDialog.findViewById(R.id.btnAddCompanyB);

        btnAddCompanyB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editComapanyNameET.getText().toString().trim().equals("")) {
                    editComapanyNameET.setError(getString(R.string.please_enter_company_name));
                } else if (editGstNumET.getText().toString().trim().equals("") && editPanNumET.getText().toString().trim().equals("")) {
                    editGstNumET.setError(getString(R.string.please_enter_at_least));
                    editPanNumET.setError(getString(R.string.please_enter_at_least));
                } else {
                    alertDialog.dismiss();
                    showToast(mActivity, "Company Added Successfully.");
                }

            }
        });
        alertDialog.show();
    }


}
