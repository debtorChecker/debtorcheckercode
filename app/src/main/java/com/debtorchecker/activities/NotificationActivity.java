package com.debtorchecker.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.debtorchecker.R;
import com.debtorchecker.adapters.CompaniesAdapter;
import com.debtorchecker.adapters.NotificationAdapter;
import com.debtorchecker.models.CompaniesModel;
import com.debtorchecker.models.NotificationModel;
import com.debtorchecker.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationActivity extends AppCompatActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = NotificationActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = NotificationActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.imgNotificationIV)
    ImageView imgNotificationIV;
    @BindView(R.id.notificationRecylerViewRV)
    RecyclerView notificationRecylerViewRV;

    NotificationAdapter mNotificationAdapter;
    ArrayList<NotificationModel> mNotificationArrayList = new ArrayList<>();


    /*
     * Activity Override method
     * #onActivityCreated
     * */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        //Butter knife
        ButterKnife.bind(this);

        setNotificationAdapter();
    }


    /*
     * Set Widgets Click lister
     * */
    @OnClick({R.id.imgBackIV, R.id.imgNotificationIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.imgNotificationIV:
                performNotificationClick();
                break;

        }
    }

    private void performNotificationClick() {

    }

    private void setNotificationAdapter() {

        for (int i = 0; i < Constants.notificationTilteArray.length; i++){
            NotificationModel mModel = new NotificationModel();
            mModel.setTitle(Constants.notificationTilteArray[i]);
            mModel.setDescription(Constants.notificationDescriptionArray[i]);

            mNotificationArrayList.add(mModel);
        }
        //Comments Adapter
        notificationRecylerViewRV.setNestedScrollingEnabled(false);
        mNotificationAdapter = new NotificationAdapter(mActivity, mNotificationArrayList);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(mActivity);
        notificationRecylerViewRV.setLayoutManager(mLayoutManagerC);
        notificationRecylerViewRV.setAdapter(mNotificationAdapter);
    }



}
