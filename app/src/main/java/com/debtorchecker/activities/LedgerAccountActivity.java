package com.debtorchecker.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.debtorchecker.R;

public class LedgerAccountActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ledger_account);
    }
}
