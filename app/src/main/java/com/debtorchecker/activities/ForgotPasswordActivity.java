package com.debtorchecker.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.debtorchecker.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ForgotPasswordActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = ForgotPasswordActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.editUserNameMobileNoET)
    EditText editUserNameMobileNoET;
    @BindView(R.id.btnForgotPassB)
    Button btnForgotPassB;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;



    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        //ButterKnife
        ButterKnife.bind(this);
    }

    /*
     * Widget Click listner
     * */
    @OnClick({R.id.btnForgotPassB,R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnForgotPassB:
                performForgotClick();
                break;
            case R.id.imgBackIV:
                onBackPressed();
                break;
        }
    }

    private void performForgotClick() {
        if (isValidate()){
            finish();
        }
    }



    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editUserNameMobileNoET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_user_name_mobile));
            flag = false;
        }
        return flag;
    }


    @Override
    public void onBackPressed() {
        finish();
    }
}
