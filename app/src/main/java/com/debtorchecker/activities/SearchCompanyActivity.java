package com.debtorchecker.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.debtorchecker.R;
import com.debtorchecker.adapters.CompaniesAdapter;
import com.debtorchecker.models.CompaniesModel;
import com.debtorchecker.utils.Constants;
import com.tooltip.OnClickListener;
import com.tooltip.Tooltip;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchCompanyActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SearchCompanyActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = SearchCompanyActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.editSearchET)
    EditText editSearchET;
    @BindView(R.id.imgAddCompanyIV)
    ImageView imgAddCompanyIV;
    @BindView(R.id.imgQuestionMarkIV)
    ImageView imgQuestionMarkIV;
    @BindView(R.id.companiesRecyclerViewRV)
    RecyclerView companiesRecyclerViewRV;


    /*
    * Initialize...Objects
    * */
    CompaniesAdapter mCompaniesAdapter;
    ArrayList<CompaniesModel> mCompaniesArrayList = new ArrayList<>();


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_company);
        //ButterKnife
        ButterKnife.bind(this);
        //Set Adapter
        setCompanyAdapter();
    }



    /*
     * Widget Click listner
     * */
    @OnClick({R.id.imgAddCompanyIV,R.id.imgQuestionMarkIV,R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgAddCompanyIV:
                performAddCompanyClick();
                break;
            case R.id.imgQuestionMarkIV:
                performQuestionMarkClick();
                break;
            case R.id.imgBackIV:
                onBackPressed();
                break;

        }
    }

    private void performQuestionMarkClick() {
        Tooltip tooltip = new Tooltip.Builder(imgQuestionMarkIV)
                .setText(getString(R.string.can_not_find_your_company))
                .setGravity(Gravity.LEFT)
                .setCornerRadius(R.dimen._5sdp)
                .setTextColor(getResources().getColor(R.color.colorWhite))
                .setBackgroundColor(getResources().getColor(R.color.colorPrimary))
                .show();

        tooltip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(@NonNull Tooltip tooltip) {
                tooltip.dismiss();
            }
        });
    }

    private void performAddCompanyClick() {
        showAddComapanyDialog();
    }


    public void showAddComapanyDialog() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_add_company);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        EditText editComapanyNameET = alertDialog.findViewById(R.id.editComapanyNameET);
        EditText editGstNumET = alertDialog.findViewById(R.id.editGstNumET);
        EditText editPanNumET = alertDialog.findViewById(R.id.editPanNumET);

        Button btnAddCompanyB = alertDialog.findViewById(R.id.btnAddCompanyB);

        btnAddCompanyB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editComapanyNameET.getText().toString().trim().equals("")) {
                    editComapanyNameET.setError(getString(R.string.please_enter_company_name));
                } else if (editGstNumET.getText().toString().trim().equals("") && editPanNumET.getText().toString().trim().equals("")) {
                    editGstNumET.setError(getString(R.string.please_enter_at_least));
                    editPanNumET.setError(getString(R.string.please_enter_at_least));
                } else {
                    alertDialog.dismiss();
                    showToast(mActivity, "Company Added Successfully.");
                }

            }
        });
        alertDialog.show();
    }


    private void setCompanyAdapter() {

        for (int i = 0; i < Constants.companiesNameArray.length; i++){
            CompaniesModel mModel = new CompaniesModel();
            mModel.setCompanyName(Constants.companiesNameArray[i]);
            mModel.setCompanyState(Constants.companiesStatesArray[i]);

            mCompaniesArrayList.add(mModel);
        }
        //Comments Adapter
        companiesRecyclerViewRV.setNestedScrollingEnabled(false);
        mCompaniesAdapter = new CompaniesAdapter(mActivity, mCompaniesArrayList);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(mActivity);
        companiesRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        companiesRecyclerViewRV.setAdapter(mCompaniesAdapter);
    }


}
