package com.debtorchecker.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.debtorchecker.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SignInActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = SignInActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.editUserNameMobileNoET)
    EditText editUserNameMobileNoET;
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.btnLoginB)
    Button btnLoginB;
    @BindView(R.id.txtForgotPassTV)
    TextView txtForgotPassTV;
    @BindView(R.id.dontHaveSignUpLL)
    LinearLayout dontHaveSignUpLL;
    @BindView(R.id.checkLoginHideShowCB)
    CheckBox checkLoginHideShowCB;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        //ButterKnife
        ButterKnife.bind(this);
        //Set up Password Hide/Show
        performPasswordHideShowClick();
    }


    /*
     * Widget Click listner
     * */
    @OnClick({R.id.btnLoginB, R.id.txtForgotPassTV, R.id.dontHaveSignUpLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLoginB:
                performLoginClick();
                break;
            case R.id.txtForgotPassTV:
                performForgotClick();
                break;
            case R.id.dontHaveSignUpLL:
                performRegisterClick();
                break;
        }
    }

    private void performLoginClick() {
        if (isValidate()){
            startActivity(new Intent(mActivity, DashboardActivity.class));
            finish();
        }
    }

    private void performForgotClick() {
        startActivity(new Intent(mActivity, ForgotPasswordActivity.class));
    }

    private void performRegisterClick() {
        startActivity(new Intent(mActivity, SignUpMobileEnterActivity.class));
    }


    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editUserNameMobileNoET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_user_name_mobile));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        }
        return flag;
    }

    /*
     *
     * Password Hide & Show
     * */
    private void performPasswordHideShowClick() {
        checkLoginHideShowCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    // show password
                    editPasswordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    // hide password
                    editPasswordET.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

    }

}
