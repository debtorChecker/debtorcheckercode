package com.debtorchecker.activities;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.debtorchecker.R;
import com.debtorchecker.adapters.ClientsAdapter;
import com.debtorchecker.models.ClientsModel;
import com.debtorchecker.utils.Constants;
import com.tooltip.OnClickListener;
import com.tooltip.Tooltip;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchInvoiceClientActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SearchInvoiceClientActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = SearchInvoiceClientActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.editSearchET)
    EditText editSearchET;
    @BindView(R.id.imgAddClientIV)
    ImageView imgAddClientIV;
    @BindView(R.id.imgQuestionMarkIV)
    ImageView imgQuestionMarkIV;
    @BindView(R.id.clientsRecyclerViewRV)
    RecyclerView clientsRecyclerViewRV;


    /*
     * Initialize...Objects
     * */
    ClientsAdapter mClientAdapter;
    ArrayList<ClientsModel> mCompaniesArrayList = new ArrayList<>();


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_client);
        //ButterKnife
        ButterKnife.bind(this);
        //Set Adapter
        setClientAdapter();
    }



    /*
     * Widget Click listner
     * */
    @OnClick({R.id.imgAddClientIV,R.id.imgQuestionMarkIV,R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgAddClientIV:
                performAddClientClick();
                break;
            case R.id.imgQuestionMarkIV:
                performQuestionMarkClick();
                break;
            case R.id.imgBackIV:
                onBackPressed();
                break;

        }
    }

    private void performQuestionMarkClick() {
        Tooltip tooltip = new Tooltip.Builder(imgQuestionMarkIV)
                .setText(getString(R.string.can_not_find_client))
                .setGravity(Gravity.LEFT)
                .setCornerRadius(R.dimen._5sdp)
                .setTextColor(getResources().getColor(R.color.colorWhite))
                .setBackgroundColor(getResources().getColor(R.color.colorPrimary))
                .show();

        tooltip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(@NonNull Tooltip tooltip) {
                tooltip.dismiss();
            }
        });
    }

    private void performAddClientClick() {
        showAddClientDialog();
    }




    public void showAddClientDialog() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_add_client);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        EditText editClientNameET = alertDialog.findViewById(R.id.editClientNameET);
        EditText editGstNumET = alertDialog.findViewById(R.id.editGstNumET);
        EditText editPanNumET = alertDialog.findViewById(R.id.editPanNumET);

        Button btnAddClientB = alertDialog.findViewById(R.id.btnAddClientB);

        btnAddClientB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editClientNameET.getText().toString().trim().equals("")) {
                    editClientNameET.setError(getString(R.string.please_enter_client_name));
                }else if (editGstNumET.getText().toString().trim().equals("") && editPanNumET.getText().toString().trim().equals("")) {
                    editGstNumET.setError(getString(R.string.please_enter_at_least));
                    editPanNumET.setError(getString(R.string.please_enter_at_least));
                } else {
                    alertDialog.dismiss();
                    showToast(mActivity, "Client Added Successfully.");
                }
            }
        });
        alertDialog.show();
    }


    private void setClientAdapter() {
        for (int i = 0; i < Constants.clientNameArray.length; i++){
            ClientsModel mModel = new ClientsModel();
            mModel.setClientName(Constants.clientNameArray[i]);
            mModel.setClientState(Constants.companiesStatesArray[i]);

            mCompaniesArrayList.add(mModel);
        }
        //Comments Adapter
        clientsRecyclerViewRV.setNestedScrollingEnabled(false);
        mClientAdapter = new ClientsAdapter(mActivity, mCompaniesArrayList);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(mActivity);
        clientsRecyclerViewRV.setLayoutManager(mLayoutManagerC);
        clientsRecyclerViewRV.setAdapter(mClientAdapter);
    }

}
