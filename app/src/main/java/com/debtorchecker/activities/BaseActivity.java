package com.debtorchecker.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.debtorchecker.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BaseActivity extends AppCompatActivity {

    /*
    * (			#   Start of group
  (?=.*\d)		#   must contains one digit from 0-9
  (?=.*[a-z])	#   must contains one lowercase characters
  (?=.*[A-Z])	#   must contains one uppercase characters
  (?=.*[@#$%])	#   must contains one special symbols in the list "@#$%"
              .	#   match anything with previous condition checking
  {6,20}	    #   length at least 6 characters and maximum of 20
  			    #    End of group
    * */
    private final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public boolean isValidPhoneNumber(String phoneNumber) {
        return phoneNumber.contains("+31") || phoneNumber.length() == 13;
    }

    /*
     * Finish the activity
     * */
    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionSlideDownExit();
    }







        /*
         * To Start the New Activity
         * */
        @Override
        public void startActivity (Intent intent){
            super.startActivity(intent);
            overridePendingTransitionSlideUPEnter();
        }

        /**
         * Overrides the pending Activity transition by performing the "Enter" animation.
         */
        public void overridePendingTransitionSlideUPEnter () {
            overridePendingTransition(R.anim.bottom_up, 0);
        }

        /**
         * Overrides the pending Activity transition by performing the "Exit" animation.
         */
        public void overridePendingTransitionSlideDownExit () {
            overridePendingTransition(0, R.anim.bottom_down);
        }

        /*
         * Show Progress Dialog
         * */
        public void showProgressDialog (Activity mActivity){
            progressDialog = new Dialog(mActivity);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.setContentView(R.layout.dialog_progress);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);

            progressDialog.show();


        }

        /*
         * Hide Progress Dialog
         * */
        public void dismissProgressDialog () {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

        /*
         * Hide Keyboard
         * */
        public boolean hideKeyBoad (Context context, View view){
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
            return true;
        }

        /*
         * Validate Email Address
         * */
        public boolean isValidEmaillId (String email){
            return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
        }

        /**
         * Validate password with regular expression
         *
         * @param password password for validation
         * @return true valid password, false invalid password
         */
        public boolean isValidPassword (String password){
            Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
            Matcher matcher = pattern.matcher(password);
            return matcher.matches();
        }


        /*
         * Share Gmail Intent
         * */
        public void shareIntentGmail (Activity mActivity){
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, mActivity.getString(R.string.share_title));
            sharingIntent.putExtra(Intent.EXTRA_TEXT, mActivity.getString(R.string.share_title) + "\n\n" + "Feedback from Xtatio");
            mActivity.startActivity(Intent.createChooser(sharingIntent, mActivity.getString(R.string.app_name)));
        }


        /*
         * Check Internet Connections
         * */
        public boolean isNetworkAvailable (Context mContext){
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }


        /*
         * Error Toast Message
         * */
        public void showToast (Activity mActivity, String strMessage){
            Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
        }


        /*
         *
         * Error Alert Dialog
         * */
        public void showAlertDialog (Activity mActivity, String strMessage){
            final Dialog alertDialog = new Dialog(mActivity);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.dialog_alert);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setCancelable(false);
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            // set the custom dialog components - text, image and button
            TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
            TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
            txtMessageTV.setText(strMessage);
            btnDismiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }


        public String formatException (Exception exception){
            String formattedString = "Internal Error";
            Log.e("BaseActivity", " -- Error: " + exception.toString());
            Log.getStackTraceString(exception);

            String temp = exception.getMessage();

            if (temp != null && temp.length() > 0) {
                formattedString = temp.split("\\(")[0];
                if (temp != null && temp.length() > 0) {
                    return formattedString;
                }
            }

            return formattedString;
        }

        /***
         *
         * @param mString this will setup to your textView
         * @param colorId  text will fill with this color.
         * @return string with color, it will append to textView.
         */
        public Spannable getColoredString (String mString,int colorId){
            Spannable spannable = new SpannableString(mString);
            spannable.setSpan(new ForegroundColorSpan(colorId), 0, spannable.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return spannable;
        }






        /*
         *
         * Convert Simple Date to Formated Date
         * */
        public String getConvertedDate (String mString){
            String[] mArray = mString.split("T");
            String formattedDate = "";
            try {
                DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd");

                // parse the date string into Date object
                Date date = srcDf.parse(mArray[0]);

                DateFormat destDf = new SimpleDateFormat("dd MMMM, yyyy");

                // format the date into another format
                formattedDate = destDf.format(date);

                System.out.println("Converted date is : " + formattedDate);
            } catch (Exception e) {
                e.toString();
            }

            return formattedDate;
        }


        //used to display only time
        public static long timeConverterLong (String strDate){
            long millisecondsSinceEpoch = 0;
            SimpleDateFormat f = new SimpleDateFormat("dd MMMM, yyyy");
            Date parseDate = null;
            try {
                parseDate = f.parse(strDate);
                millisecondsSinceEpoch = parseDate.getTime();
                return millisecondsSinceEpoch;
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return millisecondsSinceEpoch;
        }


        String[] statesArray = {
                "Andhra Pradesh",
                "Arunachal Pradesh",
                "Assam",
                "Bihar",
                "Chhattisgarh",
                "Goa",
                "Gujarat",
                "Haryana",
                "Himachal Pradesh",
                "Jammu and Kashmir",
                "Jharkhand",
                "Karnataka",
                "Kerala",
                "Madhya Pradesh",
                "Maharashtra",
                "Manipur",
                "Meghalaya",
                "Mizoram",
                "Nagaland",
                "Odisha",
                "Punjab",
                "Rajasthan",
                "Sikkim",
                "Tamil Nadu",
                "Telangana",
                "Tripura",
                "Uttarakhand",
                "Uttar Pradesh",
                "West Bengal",
                "Andaman and Nicobar Islands",
                "Chandigarh",
                "Dadra and Nagar Haveli",
                "Daman and Diu",
                "Delhi",
                "Lakshadweep",
                "Puducherry"};


        /*
         *
         * Getting All states of India
         * */

        public String[] getAllStatesOfIndia () {
            return statesArray;
        }




        /*
         * Date Picker Dialog
         * */
        public void showDatePickerDialog (Activity mActivity,final TextView mTextView){
            int mYear, mMonth, mDay, mHour, mMinute;
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            int intMonth = monthOfYear + 1;
                            mTextView.setText(getFormatedString("" + dayOfMonth) + "/" + getFormatedString("" + intMonth) + "/" + year);

                        }
                    }, mYear, mMonth, mDay);
            //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        }

        public String getFormatedString (String strTemp){
            String strActual = "";

            if (strTemp.length() == 1) {
                strActual = "0" + strTemp;
            } else if (strTemp.length() == 2) {
                strActual = strTemp;
            }

            return strActual;
        }
    }
