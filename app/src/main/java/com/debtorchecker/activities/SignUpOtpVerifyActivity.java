package com.debtorchecker.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.debtorchecker.R;
import com.debtorchecker.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpOtpVerifyActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpOtpVerifyActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = SignUpOtpVerifyActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.et1)
    EditText et1;
    @BindView(R.id.et2)
    EditText et2;
    @BindView(R.id.et3)
    EditText et3;
    @BindView(R.id.et4)
    EditText et4;
    @BindView(R.id.et5)
    EditText et5;
    @BindView(R.id.et6)
    EditText et6;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.btnNextB)
    Button btnNextB;
    @BindView(R.id.txtMobileNumberTV)
    TextView txtMobileNumberTV;
    @BindView(R.id.dontReceiveResendLL)
    LinearLayout dontReceiveResendLL;

    String strPhoneNumber = "";

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_otp_verify);
        //ButterKnife
        ButterKnife.bind(this);
        //Get Intent Data
        getIntentData();
        //Set Up EditText TextWatcher
        setUpEditTextTextWatcher();
    }

    /*
    * Getting Previous screen data
    * In this screen
    * */
    private void getIntentData() {
        if (getIntent() != null){
            strPhoneNumber = getIntent().getStringExtra(Constants.PHONE_NUMBER);
            txtMobileNumberTV.setText("sent to " + strPhoneNumber);
        }
    }


    /*
     * Widget Click listner
     * */
    @OnClick({R.id.imgBackIV, R.id.btnNextB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.btnNextB:
                performNextClick();
                break;
        }
    }

    private void performNextClick() {
        if (et1.getText().toString().trim().length() == 0 ||
                et2.getText().toString().trim().length() == 0 ||
                et3.getText().toString().trim().length() == 0 ||
                et4.getText().toString().trim().length() == 0 ||
                et5.getText().toString().trim().length() == 0 ||
                et6.getText().toString().trim().length() == 0) {
            showAlertDialog(mActivity,getString(R.string.please_enter_valid_otp));
        }else{
            startActivity(new Intent(mActivity,SignUpPasswordActivity.class));
        }
    }


    private void setUpEditTextTextWatcher() {
        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    et2.requestFocus();
                } else if (s.length() == 0) {
                    et1.clearFocus();
                }
            }
        });

        et2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    et3.requestFocus();
                } else if (s.length() == 0) {
                    et1.requestFocus();
                }
            }
        });

        et3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    et4.requestFocus();
                } else if (s.length() == 0) {
                    et2.requestFocus();
                }
            }
        });

        et4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    et5.requestFocus();
                } else if (s.length() == 0) {
                    et3.requestFocus();
                }
            }
        });

        et5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    et6.requestFocus();
                } else if (s.length() == 0) {
                    et4.requestFocus();
                }
            }
        });

        et6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    et5.requestFocus();
                }
            }
        });

    }


}
