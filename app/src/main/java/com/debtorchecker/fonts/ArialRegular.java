package com.debtorchecker.fonts;

import android.content.Context;
import android.graphics.Typeface;


public class ArialRegular {
    /*
     * Initialize the Typeface
     * */
    public static Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    public String path = "ArialRegular.ttf";
    /*
     * Initialize Context
     * */
    public Context mContext;
    /*
     * Default Constructor
     * */
    public ArialRegular() {
    }

    /*
     * Constructor with Context
     * */
    public ArialRegular(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null)
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        return fontTypeface;
    }

}