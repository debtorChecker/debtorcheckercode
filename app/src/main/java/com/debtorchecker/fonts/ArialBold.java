package com.debtorchecker.fonts;

import android.content.Context;
import android.graphics.Typeface;

public class ArialBold {
    /*
     * Initialize Context
     * */
    Context mContext;
    /*
     * Initialize the Typeface
     * */
    private Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    private String path = "ArialBold.ttf";

    /*
     * Default Constructor
     * */
    public ArialBold() {
    }

    /*
     * Constructor with Context
     * */
    public ArialBold(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null) {
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        }
        return fontTypeface;
    }

}
