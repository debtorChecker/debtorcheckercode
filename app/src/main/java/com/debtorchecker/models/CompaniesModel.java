package com.debtorchecker.models;

import java.io.Serializable;

public class CompaniesModel implements Serializable {

    String companyName = "";
    String companyAddress = "";
    String comapnayDistrict = "";
    String companyState = "";

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getComapnayDistrict() {
        return comapnayDistrict;
    }

    public void setComapnayDistrict(String comapnayDistrict) {
        this.comapnayDistrict = comapnayDistrict;
    }

    public String getCompanyState() {
        return companyState;
    }

    public void setCompanyState(String companyState) {
        this.companyState = companyState;
    }
}
