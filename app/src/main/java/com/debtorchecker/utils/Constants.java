package com.debtorchecker.utils;

public class Constants {



 public static final String TYPE_TERM_CONTIONS = "type_term_condition";
 public static final String TYPE_SIGN_UP = "type_signup";
 public static final String TYPE_NAV_MENU = "type_nav_menu";

 public static final String PHONE_NUMBER = "number";
 public static final String companiesNameArray[] = {"Infosys","TechMahindra","Technoly Sys","Sql Sys","Kindal Bit Technologies","Udyog Tech Park"};
 public static final String clientNameArray[] = {"Aaron Jack","Pooja Rehaan","Reena Mangal","Gunjan Kour","Diya Mirza","Aarushi Rotela"};
 public static final String companiesStatesArray[] = {"Haryana","Himachal Pradesh","Jammu and Kashmir","Maharashtra","Nagaland","Nagaland","Manipur"};

 public static final String notificationTilteArray[] = {"Invoice Added","Reminder Added","Client Added","Market Research Requested","Ledger Updated"};
 public static final String notificationDescriptionArray[] = {"Aaron Jack  Added Invoice","Jiya Roy  Added Reminder","Rakesh Added Client","Aaron Jack Requested Market Research ","Riya Roy Updated his Ledger "};

}
